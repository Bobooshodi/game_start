package cct.mad.lab;

import java.util.Random;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;


public class Sprite {
    
	//x,y position of sprite - initial position (0,50)
	int RandomNumber;
	private int x = 0; 
	private int y = 50;
    private int xSpeed = 5;//Horizontal increment of position (speed)
    private int ySpeed = 5;// Vertical increment of position (speed)
    private GameView gameView;
    private Bitmap spritebmp, spritebmp1;
    //Width and Height of the Sprite image
    private int bmp_width;
	private int bmp_height;
	private static final int ANIMATION_ROWS = 4;
	private static final int ANIMATION_COLUMNS = 4;
	private int currentFrame = 0;

    // Needed for new random coordinates.
  	private Random random = new Random();
    
    public Sprite(GameView gameView) {
          this.gameView=gameView;
          /**spritebmp = BitmapFactory.decodeResource(gameView.getResources(),
                  R.drawable.flyingbird);
          this.bmp_width = spritebmp.getWidth();
  		  this.bmp_height= spritebmp.getHeight();*/
  		  
  		spritebmp = BitmapFactory.decodeResource(gameView.getResources(),
				R.drawable.bird);
  		spritebmp1 = BitmapFactory.decodeResource(gameView.getResources(),
				R.drawable.bird);
this.bmp_width = spritebmp.getWidth()/ANIMATION_COLUMNS;
this.bmp_height= spritebmp.getHeight()/ANIMATION_ROWS;

     }
    //update the position of the sprite
    public void update() {   	
    		Random generator = new Random();
	     y = generator.nextInt(450) + 1;	
    	x = generator.nextInt(350) + 1;
	     if (x >= gameView.getWidth() - bmp_width - xSpeed || x + xSpeed <= 0) {
             xSpeed = -xSpeed;
      } 
    	x = x + xSpeed;
    	
    	if (y >= gameView.getHeight() - bmp_height - ySpeed || y + ySpeed <= 0) {
            ySpeed = -ySpeed;
     }
    	y = y + ySpeed;
        wrapAround(); //Adjust motion of sprite.
        currentFrame = ++currentFrame % ANIMATION_COLUMNS;
    }

    public void draw(Canvas canvas) {
    	
    	int srcX = currentFrame * bmp_width;//frame - x direction
    	   int srcY; 					//row
    	   if (xSpeed > 0){//Sprite going right; row = 0
    	     		srcY = 1 * bmp_height;
    	   }
    	   else { //Going left; row = 1
    	        	srcY = 0 * bmp_height;
    	   }
    	   //Create Rect around the source image to be drawn
    	   Rect src = new Rect(srcX, srcY, srcX+bmp_width, srcY + bmp_height);
    	   //Rect for destination image
    	   Rect dst = new Rect(x, y, x + bmp_width, y + bmp_height);
    	   //draw the image frame
    	   canvas.drawBitmap(spritebmp, src, dst, null);

       	
    	//Draw sprite image
       //	canvas.drawBitmap(spritebmp, x , y, null);
    }
    
    public void wrapAround(){
    	//Code to wrap around	
      	if (x < 0) x = x + gameView.getWidth(); //increment x whilst not off screen
    	if (x >= gameView.getWidth()){ //if gone of the right sides of screen
    			x = x - gameView.getWidth(); //Reset x
    	}
    	if (y < 0) y = y + gameView.getHeight();//increment y whilst not off screen
    	if (y >= gameView.getHeight()){//if gone of the bottom of screen
    		y -= gameView.getHeight();//Reset y
    	}
    }
  
    /* Checks if the Sprite was touched. */
    public boolean wasItTouched(float ex, float ey){
    	boolean touched = false; 
    	if ((x <= ex) && (ex < x + bmp_width) &&
    		(y <= ey) && (ey < y + bmp_height)) {
          		touched = true;
    	}
    	return touched;
    }//End of wasItTouched

}  
