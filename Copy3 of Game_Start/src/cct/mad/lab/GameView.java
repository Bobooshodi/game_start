package cct.mad.lab;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * This class takes care of surface for drawing and touches
 * 
 */

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

	/* Member (state) fields   */
	private GameLoopThread gameLoopThread;
	private Paint paint; //Reference a paint object 
    /** The drawable to use as the background of the animation canvas */
    private Bitmap mBackgroundImage;
    private Bitmap spriteImage;
    private Sprite sprite;
    private int hitCount;
    /* For the countdown timer */
    private long  startTime ;			//Timer to count down from
    private final long interval = 1 * 1000; 	//1 sec interval
    private CountDownTimer countDownTimer; 	//Reference to class
    private boolean timerRunning = false;
    private String displayTime; 		//To display time on the screen
    private boolean gameOver;
    String scores = GameActivity.sendScore();
    private ArrayList<Sprite> spritesArrayList;
    SoundPool sp;
	 MediaPlayer mp;
	 int shot = 0;
	 int arraySize;
  
	public GameView(Context context) {
		super(context);
		// Focus must be on GameView so that events can be handled.
		
		mBackgroundImage = BitmapFactory.decodeResource(this.getResources(), R.drawable.background);
		spriteImage = BitmapFactory.decodeResource(this.getResources(), R.drawable.flyingbird);
		spritesArrayList= new ArrayList<Sprite>();
		
		this.setFocusable(true);
		// For intercepting events on the surface.
		this.getHolder().addCallback(this);
	}
	 /* Called immediately after the surface created */
	public void surfaceCreated(SurfaceHolder holder) {
		sprite = new Sprite(this);
		for(int i = 0; i<5; i++){
			spritesArrayList.add(new Sprite(this));
		}
		mBackgroundImage = Bitmap.createScaledBitmap(mBackgroundImage, getWidth(), getHeight(), true);
		// We can now safely setup the game start the game loop.
		ResetGame();//Set up a new game up - could be called by a 'play again option'
		gameLoopThread = new GameLoopThread(this.getHolder(), this);
		gameLoopThread.running = true;
		gameLoopThread.start();
	}
		
	//To initialise/reset game
	private void ResetGame(){
		/* Set paint details */
		sprite = new Sprite(this);
	    paint = new Paint();
		paint.setColor(Color.WHITE); 
		paint.setTextSize(20); 
		hitCount = 0;
		//Set timer
		startTime = 60;//Start at 10s to count down
		//Create new object - convert startTime to milliseconds
		countDownTimer=new MyCountDownTimer(startTime*1000,interval);
		countDownTimer.start();//Start it running
		timerRunning = true;
		gameOver = false;

	}
	
	private class MyCountDownTimer extends CountDownTimer {

		  public MyCountDownTimer(long startTime, long interval) {
				super(startTime, interval);
		  }
		  public void onFinish() {
				displayTime = "Times Over!";
				timerRunning = false;
				countDownTimer.cancel();
				gameOver = true;
		  }
		  public void onTick(long millisUntilFinished) {
				displayTime = " " + millisUntilFinished / 1000;
		  }
		}//End of MyCountDownTimer

	
	  public void GetArrayListSize() {
	  if(spritesArrayList.isEmpty()) {
	  
	  }
	  else {
	   arraySize = spritesArrayList.size();
	  }
	  Log.i("arraysize",""+spritesArrayList.size());
	  }
	 
	
	//This class updates and manages the assets prior to drawing - called from the Thread
	public void update(){
			sprite.update();
			for (int i=0; i<arraySize; i++){
			 spritesArrayList.get(i).update();
			}
	}
	/**
	 * To draw the game to the screen
	 * This is called from Thread, so synchronisation can be done
	 */
	public void doDraw(Canvas canvas) {
		
		if (gameOver == true){
			canvas.drawText("GAME OVER", 175, 400, paint);
			}
		else {
		canvas.drawBitmap(mBackgroundImage, 0, 0, null);
		//Draw all the objects on the canvas
		canvas.drawText("Hit Count: " + hitCount,5,25, paint);
		canvas.drawText("Timer: " + displayTime,350,25, paint);
		canvas.drawText("Score to Beat: " + GameActivity.sendScore(), 130, 25, paint);
		
		GetArrayListSize();
		 Log.i("arraysize for drawing",""+arraySize);
		for (int i = 0; i<5; i++){
		spritesArrayList.get(i).draw(canvas);
		//sprite.draw(canvas);
		}
		}
	}
	
	//To be used if we need to find where screen was touched
	public boolean onTouchEvent(MotionEvent event) {
		if (sprite.wasItTouched(event.getX(), event.getY())){
			/* For now, just renew the Sprite */
					sprite = new Sprite(this);
			    	   	hitCount++; 	
			    	    if(shot != 0){
			            	//Check parameters of play method
			                sp.play(shot, 1, 1, 0,0, 1);
			                mp.stop();
			             }
			    	    for (int i=0; i<5; i++){
			    	    	if (spritesArrayList.get(i).wasItTouched(event.getX(), getY())) {
			    	    		spritesArrayList.set(i, new Sprite(this));
			    	    		hitCount++;
			    	    	}
			    	    }
			    	}

		return true;
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		gameLoopThread.running = false;
		
		// Shut down the game loop thread cleanly.
		boolean retry = true;
		while(retry) {
			try {
				gameLoopThread.join();
				retry = false;
			} catch (InterruptedException e) {}
		}
	}
	
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		
	}

	public int getHitCount() {
		return hitCount;
	}
}
