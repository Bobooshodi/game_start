package cct.mad.lab;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainMenu extends Activity {

	private static final int SCORE_REQUEST_CODE = 1;// The request code for the intent
	FileOutputStream outputStream;//To write bytes to a file
	public static final String FILE_NAME = "MyFile"; //stored in cct.mad.lab/data/data

	TextView tvScore;
	String score;
	int highScoreString;
	Intent gameIntent;
	int highScore;
	SoundPool sp;
	 MediaPlayer mp;
	 int scoreFromGame = 0;
		
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_start);
		
		sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
	    mp = MediaPlayer.create(this, R.raw.background);
	    mp.setLooping(true);
	    mp.start();
		
		tvScore = (TextView)findViewById(R.id.tvGuessGame);
		
		//highScore = Integer.parseInt(tvScore.getText().toString());
		
		InputStream inputStream = null;
		try {// to open the file
			inputStream = openFileInput(FILE_NAME); 
			try {//to read the file
				InputStreamReader inputStreamReader = new InputStreamReader(
						inputStream);
				BufferedReader bufferedReader = new BufferedReader(
						inputStreamReader);
				String inputString;
				StringBuffer stringBuffer = new StringBuffer();
				while ((inputString = bufferedReader.readLine()) != null) {
					stringBuffer.append(inputString + "\n");
				}// End of reading file
				Toast.makeText(MainMenu.this,
						"File contents = \n" + stringBuffer.toString(),
						Toast.LENGTH_LONG).show();
				//highScore = Integer.parseInt(tvScore.getText().toString());
				highScoreString = Integer.parseInt(stringBuffer.toString().trim());
				
				tvScore.setText(stringBuffer.toString());				
				
				inputStream.close();
			} catch (IOException e) {// Problem reading file
				Log.e("TAG", "Can not read file: " + e.toString());
			}
		} catch (FileNotFoundException e1) {
			Log.e("TAG", "File not found: " + e1.toString());
		}
	}
	
	
	public void startGame(View v){
		gameIntent = new Intent(this,GameActivity.class);
		gameIntent.putExtra("Score", tvScore.getText().toString());
	    startActivityForResult(gameIntent, SCORE_REQUEST_CODE );  
	}
    /* Create Options Menu */
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	// Respond to item selected on OPTIONS MENU
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		//put data in Intent
		case R.id.easy:
			Toast.makeText(this, "Easy chosen", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.medium:
			Toast.makeText(this, "Medium chosen", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.hard:
			Toast.makeText(this, "Hard chosen", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.other:
			Toast.makeText(this, "Other chosen", Toast.LENGTH_SHORT).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent retIntent) {
		//highScore = Integer.parseInt(tvScore.getText().toString());
	    // Check which request we're responding to
	    if (requestCode == SCORE_REQUEST_CODE) {
	        // Make sure the request was successful
	        if (resultCode == RESULT_OK) {
	        	if (retIntent.hasExtra("GAME_SCORE")) {
					scoreFromGame = retIntent.getExtras().getInt("GAME_SCORE");
					tvScore.setText(Integer.toString(scoreFromGame));
	        	}
	        }
	        if (scoreFromGame > highScoreString){
	        String savedScore = Integer.toString(scoreFromGame);
	        String newLine = "\n";
			try {
				outputStream = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
				outputStream.write(savedScore.getBytes());//Write bytes
				outputStream.write(newLine.getBytes());
				//outputStream.write(sNum.getBytes());
				outputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			File fPath = getFilesDir();//For dir path of the file
			Toast.makeText(MainMenu.this,
					"Files saved to : " + fPath.toString() + "/" + FILE_NAME,
					Toast.LENGTH_LONG).show();
    	}
	    
	    
	    
	     String savedScore = Integer.toString(scoreFromGame);
 		String parameters = "HighScore="+savedScore; 
		WebPageTask task = new WebPageTask();
		//External
		 // task.execute(new String[] { "http://www.zilmat.com/cwd/genresponse.php?"+parameters});
		task.execute(new String[] { "http://192.168.1.11/ISD/testing.php?"+parameters});
		//Internal
		//task.execute(new String[] { "http://www.inn.leedsmet.ac.uk/~ssharpe/genresponse.php?"+parameters});
	    }
	}
	
	private class WebPageTask extends AsyncTask<String, Void, String> {
		
		   protected String doInBackground(String... urls) {   
		            // params comes from the execute() call: urls[0] is the first url in the array.
		            try {
		                return downloadUrl(urls[0]);
		            } catch (IOException e) {
		                return "Unable to retrieve web page. URL may be invalid.";
		            }
		    }
			@Override
			protected void onPostExecute(String result) {
				String Score = Integer.toString(scoreFromGame);
				//if (Score.equals(result)){
					Toast.makeText(MainMenu.this, result, Toast.LENGTH_LONG).show();
			//	}
			/**else if (scoreFromGame > Integer.parseInt(result)){
					String savedScore = Integer.toString(scoreFromGame);
			 		String parameters = "HighScore="+savedScore; 
					WebPageTask task = new WebPageTask();
					//External
					 // task.execute(new String[] { "http://www.zilmat.com/cwd/genresponse.php?"+parameters});
					task.execute(new String[] { "http://192.168.1.11/ISD/testing.php?"+parameters});
					//Internal
					//task.execute(new String[] { "http://www.inn.leedsmet.ac.uk/~ssharpe/genresponse.php?"+parameters});
				    }*/
				
			}
			
			private String downloadUrl(String myurl) throws IOException {
			    InputStream is = null;	        
			    try {
			        URL url = new URL(myurl);
			        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			        conn.setReadTimeout(10000 /* milliseconds */);
			        conn.setConnectTimeout(15000 /* milliseconds */);
			        conn.setRequestMethod("GET");
			        conn.setDoInput(true);
			        // Starts the query
			        conn.connect();
				    //Create the input stream to read the response
			        is = conn.getInputStream();
			        // Convert the InputStream into a string
			        String contentAsString= "";
					BufferedReader buffer = new BufferedReader(new InputStreamReader(is, "UTF-8"));
					String s = "";
					while ((s = buffer.readLine()) != null) {
						contentAsString += s;
					}			    
			        return contentAsString;		        
			    // Makes sure that the InputStream is closed after the app is finished using it.
			    } finally {
			        if (is != null) {
			            is.close();
			        } 
			    }
			}//End of downloadUrl
	}
	}