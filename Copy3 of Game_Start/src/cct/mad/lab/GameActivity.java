package cct.mad.lab;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/* Activity class for the game.
 * Creates the GameView and can pass parameters to the gameView.
 * Can also receive/return Intent data to configure game and report game status(eg score)
 */
public class GameActivity extends Activity {

	GameView gameView;// Reference the gameView
	String valueReceived;
	public static String score;
	SoundPool sp;
	 MediaPlayer mp;
	 int shot = 0;

	protected void onCreate(Bundle bundle) {

		super.onCreate(bundle);
		// remove title bar
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// Set the layout
		setContentView(R.layout.game_view_container);
		// Get the id of the layout
		RelativeLayout mainscreen = (RelativeLayout) findViewById(R.id.mainscreen);
		// Make the GameView
		gameView = new GameView(this);
		// Get  data from intent and config gameView here

		gameView.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT));
		// Add GameView
		mainscreen.addView(gameView);
		
		sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
	    shot = sp.load(this, R.raw.gunshot, 1);
	    mp = MediaPlayer.create(this, R.raw.background);
	    mp.setLooping(true);
	    mp.start();
		
		Bundle extras = getIntent().getExtras();
	    if (extras != null) {
		    valueReceived = extras.getString("Score");
		    if (valueReceived  != null) {
		      score = valueReceived;
		    }  
		 }
	}
	
	public static String sendScore(){
		return score;
	}
	/* Called when activity is done and should be closed. 
	 * The ActivityResult is propagated back to whoever launched via onActivityResult()
	 */
	
	public void finish(){
		  Intent returnIntent = new Intent();
		  returnIntent.putExtra("GAME_SCORE",gameView.getHitCount());
		  setResult(RESULT_OK, returnIntent);
		  super.finish();
		}
}
