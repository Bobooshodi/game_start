package cct.mad.lab;

import java.util.Calendar;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class InstanceState extends Activity {

	// Created when activity first starts
	private Calendar startTime = Calendar.getInstance();
	private TextView tvOnSave;
	private TextView tvOnRestore;
	private TextView tvStartTime;
	private int saveCount, restoreCount;
	
	//Automatically called when visible Activity is created or re-created
	public void onCreate(Bundle state) {
		super.onCreate(state);
	//	setContentView(R.layout.instance);
	//	tvStartTime = (TextView) findViewById(R.id.tvStartTime);
	//	tvOnSave = (TextView) findViewById(R.id.tvSaveCount);
	//	tvOnRestore = (TextView) findViewById(R.id.tvRestoreCount);
		// See if the state has already been saved
		if ((state != null)) { // See if there is anything in the bundle
			if (state.containsKey("savecount")) {
				saveCount = state.getInt("savecount");
			}
			if (state.containsKey("restorecount")) {
				restoreCount = state.getInt("restorecount");
			}
			// This is not really needed as View data (TextView) is saved automatically
			if (state.containsKey("starttime")) {
				startTime = (Calendar) state.getSerializable("starttime");
			}
		}
		restoreCount++;
		tvOnRestore.setText("Number of Restores: "+ String.valueOf(restoreCount));
		tvOnSave.setText("Number of Saves: " + String.valueOf(saveCount));
		String sTime = startTime.getTime().toString();
		tvStartTime.setText("Activity started :" + sTime);
	}
   //Automatically called when visible Activity is destroyed
	protected void onSaveInstanceState(Bundle outState) {
		saveCount++;// Increment number of saves
		//Save additional data
		outState.putSerializable("starttime", startTime);
		outState.putInt("savecount", saveCount);
		outState.putInt("restorecount", restoreCount);
		super.onSaveInstanceState(outState);
	}
	
	 public void onPause(View v) {
         super.onPause();
         Toast.makeText(this, "onPause()", Toast.LENGTH_LONG).show();
       //  this.pause();
 }
	 public void onResume(View v) {
         super.onResume();
         Toast.makeText(this, "onResume()", Toast.LENGTH_LONG).show();
        
   }
	
}
